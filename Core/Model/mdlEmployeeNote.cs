﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlEmployeeNote
    {
        [DataMember]
        public int NoteID { get; set; }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string EmployeeID { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string VisitID { get; set; }

        [DataMember]
        public string Note { get; set; }
    }

    //==================================================
    //service param from handheld and service result for mobile
    public class mdlEmployeeNoteParam
    {
        [DataMember]
        public int noteID { get; set; }

        [DataMember]
        public string date { get; set; }

        [DataMember]
        public string employeeID { get; set; }

        [DataMember]
        public string employeeName { get; set; }

        [DataMember]
        public string customerID { get; set; }

        [DataMember]
        public string visitID { get; set; }

        [DataMember]
        public string note { get; set; }
    }

}
