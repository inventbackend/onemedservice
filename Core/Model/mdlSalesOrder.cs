﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlSalesOrderHeader
    {
        [DataMember]
        public int SalesOrderID { get; set; }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string EmployeeID { get; set; }

        [DataMember]
        public decimal PaymentAmount { get; set; }
    }

//==================================================

    public class mdlSalesOrderDetail
    {
        [DataMember]
        public int SalesOrderID { get; set; } 

        [DataMember]
        public int Line { get; set; }

        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public string UOM { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }

//==================================================
    //service param from handheld and service result for mobile
    public class mdlSalesOrderParam
    {
        //header model
        [DataMember]
        public int salesOrderID { get; set; }

        [DataMember]
        public string customerID { get; set; }

        [DataMember]
        public string employeeID { get; set; }

        [DataMember]
        public string branchID { get; set; }

        [DataMember]
        public string deviceID { get; set; }

        [DataMember]
        public decimal paymentAmount { get; set; }

        //detail model
        [DataMember]
        public List<mdlSalesOrderDetailParam>  item { get; set; }
    }

//==================================================

    public class mdlSalesOrderDetailParam
    {
        [DataMember]
        public int line { get; set; }

        [DataMember]
        public string productID { get; set; }

        [DataMember]
        public int qty { get; set; }

        [DataMember]
        public string uom { get; set; }

        [DataMember]
        public decimal price { get; set; }
    }

}
