﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlProductPrice
    {
        [DataMember]
        public string productID { get; set; }

        [DataMember]
        public string productType { get; set; }

        [DataMember]
        public string productGroup { get; set; }

        [DataMember]
        public string productWeight { get; set; }
        
        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public string branchID { get; set; }

        [DataMember]
        public string uom { get; set; }

        [DataMember]
        public decimal price { get; set; }
    }

}
