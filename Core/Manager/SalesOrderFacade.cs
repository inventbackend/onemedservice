﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Core.Manager
{
    public class SalesOrderFacade : Base.Manager
    {
        public static int LoadSalesOrderID()
        {
            int salesOrderID = 1;

            try
            {    
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                };

                string sql = "SELECT TOP 1 SalesOrderID FROM Sales_Order ORDER BY SalesOrderID DESC";
                DataTable dt = DataFacade.DTSQLCommand(sql, sp);

                foreach (DataRow row in dt.Rows)
                {
                    salesOrderID = Convert.ToInt32(row["SalesOrderID"].ToString()) + 1;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return salesOrderID;
        }

        public static Model.mdlSalesOrderParam LoadSalesOrderByID(int SalesOrderID)
        {
            Model.mdlSalesOrderParam mdlSalesOrderParam = new Model.mdlSalesOrderParam();
            mdlSalesOrderParam.item = new List<Model.mdlSalesOrderDetailParam>();
            
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@SalesOrderID", SqlDbType = SqlDbType.Int, Value = SalesOrderID }
                };

                string sql = @"SELECT a.SalesOrderID,a.Date,a.CustomerID,a.EmployeeID,a.PaymentAmount, 
                b.Line, b.ProductID, b.Qty, b.UOM, b.Price
                FROM Sales_Order a
                INNER JOIN Sales_Order_Detail b ON b.SalesOrderID = a.SalesOrderID
                WHERE a.SalesOrderID = @SalesOrderID
                ORDER BY b.Line";
                DataTable dt = DataFacade.DTSQLCommand(sql, sp);

                int i = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Model.mdlSalesOrderDetailParam mdlSalesOrderDetailParam = new Model.mdlSalesOrderDetailParam();
                    if (i == 0)
                    {
                        mdlSalesOrderParam.salesOrderID = Convert.ToInt32(row["SalesOrderID"].ToString());
                        mdlSalesOrderParam.customerID = row["CustomerID"].ToString();
                        mdlSalesOrderParam.employeeID = row["EmployeeID"].ToString();
                        mdlSalesOrderParam.paymentAmount = Convert.ToDecimal(row["PaymentAmount"].ToString());

                        mdlSalesOrderDetailParam.line = Convert.ToInt32(row["Line"].ToString());
                        mdlSalesOrderDetailParam.productID = row["ProductID"].ToString();
                        mdlSalesOrderDetailParam.qty = Convert.ToInt32(row["Qty"].ToString());
                        mdlSalesOrderDetailParam.uom = row["UOM"].ToString();
                        mdlSalesOrderDetailParam.price = Convert.ToDecimal(row["Price"].ToString());

                        mdlSalesOrderParam.item.Add(mdlSalesOrderDetailParam);
                    }
                    else
                    {
                        mdlSalesOrderDetailParam.line = Convert.ToInt32(row["Line"].ToString());
                        mdlSalesOrderDetailParam.productID = row["ProductID"].ToString();
                        mdlSalesOrderDetailParam.qty = Convert.ToInt32(row["Qty"].ToString());
                        mdlSalesOrderDetailParam.uom = row["UOM"].ToString();
                        mdlSalesOrderDetailParam.price = Convert.ToDecimal(row["Price"].ToString());

                        mdlSalesOrderParam.item.Add(mdlSalesOrderDetailParam);
                    }

                    i++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return mdlSalesOrderParam;
        }

        public static Model.mdlResultSvc InsertSalesOrder(Model.mdlSalesOrderParam lParam)
        {
            var mdlResultSvc = new Model.mdlResultSvc();
            int tempSalesOrderID = 0;

            try
            {
                var mdlResultInsertHeader = new Model.mdlResult();
                var mdlResultInsertDetail = new Model.mdlResult();

                var listSalesOrderHeader = new List<Model.mdlSalesOrderHeader>();
                var listSalesOrderDetail = new List<Model.mdlSalesOrderDetail>();

                //insert sales order header
                var SOH = new Model.mdlSalesOrderHeader();
                DateTime dateTime = DateTime.Now;

                SOH.SalesOrderID = LoadSalesOrderID();
                SOH.Date = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                SOH.CustomerID = lParam.customerID;
                SOH.EmployeeID = lParam.employeeID;
                SOH.PaymentAmount = lParam.paymentAmount;

                listSalesOrderHeader.Add(SOH);

                mdlResultInsertHeader.Result = Manager.DataFacade.DTSQLListInsert(listSalesOrderHeader, "Sales_Order");
                tempSalesOrderID = SOH.SalesOrderID;

                //insert sales order detail
                foreach (var param in lParam.item)
                {
                    var SOD = new Model.mdlSalesOrderDetail();

                    SOD.SalesOrderID = tempSalesOrderID;
                    SOD.Line = param.line;
                    SOD.ProductID = param.productID;
                    SOD.Qty = param.qty;
                    SOD.UOM = param.uom;
                    SOD.Price = param.price;

                    listSalesOrderDetail.Add(SOD);
                }

                mdlResultInsertDetail.Result = Manager.DataFacade.DTSQLListInsert(listSalesOrderDetail, "Sales_Order_Detail");
                lParam.salesOrderID = tempSalesOrderID;

                if (mdlResultInsertHeader.Result == "1" && mdlResultInsertDetail.Result == "1")
                {
                    mdlResultSvc.Title = "InsertSalesOrder";
                    mdlResultSvc.StatusCode = "00";
                    mdlResultSvc.StatusMessage = "Berhasil";
                    mdlResultSvc.ValueParameter = lParam;
                }
                else if (mdlResultInsertHeader.Result == "0")
                {
                    mdlResultSvc.Title = "InsertSalesOrder";
                    mdlResultSvc.StatusCode = "01";
                    mdlResultSvc.StatusMessage = "Gagal Insert Sales Order Header";
                    mdlResultSvc.ValueParameter = lParam;
                }
                else
                {
                    mdlResultSvc.Title = "InsertSalesOrder";
                    mdlResultSvc.StatusCode = "02";
                    mdlResultSvc.StatusMessage = "Gagal Insert Sales Order Detail";
                    mdlResultSvc.ValueParameter = lParam;
                }
            }
            catch(Exception e)
            {
                mdlResultSvc.Title = "InsertSalesOrder";
                mdlResultSvc.StatusCode = "03";
                mdlResultSvc.StatusMessage = "Gagal memanggil servis";
                mdlResultSvc.ValueParameter = lParam;
            }

            return mdlResultSvc;
        }

    }
}
